const express = require('express');
const router = express.Router();
const pacientesController = require('../controllers/pacientes');

/* GET  */
router.get('/', pacientesController.getPacientes);
router.get('/:id([0-9]+)', pacientesController.getPacienteById);
router.get('/evoluciones/:id([0-9]+)', pacientesController.getEvolucionesByPaciente);

/* POST  */
router.post('/', pacientesController.addPaciente);
router.post('/:id_paciente([0-9]+)/profesionales/:id_profesional([0-9]+)', pacientesController.addPacienteToProfesional);

/* DELETE  */
router.delete('/:id([0-9]+)', pacientesController.deletePaciente);

/* PUT  */
router.put('/:id([0-9]+)', pacientesController.updatePaciente);

module.exports = router;