const express = require('express');
const router = express.Router();
const profesionalesController = require('../controllers/profesionales');

/* GET */

router.get('/', profesionalesController.getProfesionales);
router.get('/:id([0-9]+)', profesionalesController.getProfesionalById);
router.get('/:id([0-9]+)/pacientes', profesionalesController.getPacientesByProfesionalId);
router.get('/pacientes', profesionalesController.getPacientesWithProfesional)
router.get('/pacientes/evoluciones/:id([0-9]+)', profesionalesController.getEvolucionesByProfesionalId);

/* POST  */
router.post('/', profesionalesController.addProfesional);

/* DELETE  */
router.delete('/:id([0-9]+)', profesionalesController.deleteProfesional);

/* PUT */
router.put('/:id([0-9]+)', profesionalesController.updateProfesional);


module.exports = router;