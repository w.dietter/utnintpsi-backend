const Paciente = require("../models/Paciente");
const createResponse = require("../models/createResponse");
const chalk = require("chalk");

/* GET all pacientes /pacientes */
exports.getPacientes = (req, res, next) => {
  Paciente.getPacientes()
    .then(results => {
      const pacientes = results[0];
      return createResponse(
        res,
        200,
        "Se encontraron los pacientes correctamente",
        "pacientes",
        pacientes
      );
    })
    .catch(err => {
      return createResponse(
        res,
        404,
        "No se encontraron los pacientes",
        "err",
        err.message
      );
    });
};

/* GET paciente by id /pacientes/:id */
exports.getPacienteById = (req, res, next) => {
  const id = parseInt(req.params.id);
  Paciente.getPacienteById(id)
    .then(results => {
      const paciente = results[0];
      return createResponse(
        res,
        200,
        `Se encontro el paciente con id: ${id}`,
        "paciente",
        paciente
      );
    })
    .catch(err => {
      console.log(chalk.bgRed(err));
      return createResponse(
        res,
        404,
        "No se pudo encontrar el paciente",
        "error",
        err.message
      );
    });
};

/* GET get evoluciones by paciente id */
exports.getEvolucionesByPaciente = (req, res, next) => {
  const { id } = req.params;
  Paciente.getEvolucionesByPaciente(id)
    .then(results => {
      const evoluciones = results[0];
      return createResponse(
        res,
        200,
        "Se encontraron exitosamente las evoluciones.",
        "evoluciones",
        evoluciones
      );
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};

/* POST add paciente /pacientes */
exports.addPaciente = (req, res, next) => {
  const { nombre, apellido, edad, telefono } = req.body;
  Paciente.addPaciente(nombre, apellido, edad, telefono)
    .then(results => {
      if (results[0].affectedRows > 0) {
        return createResponse(
          res,
          200,
          "Paciente agregado exitosamente.",
          "queryResults",
          results[0]
        );
      } else {
        throw new Error("Error al agregar el paciente");
      }
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};

/* POST add paciente to Profesional /pacientes/:id/profesionales/:id */
exports.addPacienteToProfesional = (req, res, next) => {
  const { id_paciente, id_profesional } = req.params;
  Paciente.addPacienteToProfesional(id_paciente, id_profesional)
    .then(results => {
      return createResponse(
        res,
        200,
        "Paciente agregado exitosamente.",
        "queryResults",
        results[0]
      );
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};

/* DELETE delete paciente /pacientes/:id */
exports.deletePaciente = (req, res, next) => {
  const { id } = req.params;
  Paciente.deletePaciente(id)
    .then(results => {
      if (results[0].affectedRows > 0) {
        return createResponse(
          res,
          200,
          "Se borro el paciente exitosamente",
          "queryResults",
          results[0]
        );
      } else {
        throw new Error("No se pudo borrar el paciente.");
      }
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};

/* PUT update paciente /pacientes/:id */
exports.updatePaciente = (req, res, next) => {
  const id = parseInt(req.params.id);
  const { nombre, apellido, edad, telefono } = req.body;
  Paciente.updatePaciente(id, nombre, apellido, edad, telefono)
    .then(results => {
      if (results[0].affectedRows > 0) {
        return createResponse(
          res,
          200,
          `Paciente con id: ${id} ha sido modificado exitosamente`,
          "queryResults",
          results[0]
        );
      } else {
        throw new Error("Error al modificar el paciente con id: " + id);
      }
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};
