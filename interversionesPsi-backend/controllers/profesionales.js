const Profesional = require("../models/Profesional");
const createResponse = require("../models/createResponse");

/* GET all profesionales /profesionales */
exports.getProfesionales = (req, res, next) => {
  Profesional.getProfesionales()
    .then(results => {
      const profesionales = results[0];
      return createResponse(
        res,
        200,
        "Profesionales encontrados exitosamente",
        "profesionales",
        profesionales
      );
    })
    .catch(err => {
      return createResponse(
        res,
        404,
        "No se pudieron encontrar los profesionales",
        "error",
        err
      );
    });
};

/* GET profesional by id /profesionales/:id */
exports.getProfesionalById = (req, res, next) => {
  const id = parseInt(req.params.id);
  Profesional.getProfesionalById(id)
    .then(results => {
      const profesional = results[0];
      if (profesional.length) {
        return createResponse(
          res,
          200,
          "Profesional encontrado correctamente.",
          "profesional",
          profesional
        );
      } else {
        throw new Error("Profesional con id: " + id + " no encontrado.");
      }
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};

/* GET pacientes with profesional /profesionales/pacientes */
exports.getPacientesWithProfesional = (req, res, next) => {
  Profesional.getPacientesWithProfesional() 
    .then(results => {
      const pacientes = results[0];
      if(pacientes.length > 0) {
        return createResponse(res, 200, "Pacientes que se atienden con algun profesional encontrados correctamente.", "pacientes", pacientes);
      } else {
        throw new Error("No se encontraron pacientes que se atiendan con algun profesional.");
      }
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "err", err);
    }) 
} 

/* GET get pacientes by profesional id /profesionales/:id/pacientes */
exports.getPacientesByProfesionalId = (req, res, next) => {
  const id = parseInt(req.params.id);
  Profesional.getPacientesByProfesionalId(id)
    .then(results => {
      const pacientes = results[0];
      if (pacientes.length > 0) {
        return createResponse(
          res,
          200,
          "Pacientes encontrados correctamente.",
          "pacientes",
          pacientes
        );
      } else {
        throw new Error(
          "No se encontraron pacientes para el profesional con id: " + id
        );
      }
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};

/* GET get evoluciones by profesional id */
exports.getEvolucionesByProfesionalId = (req, res, next) => {
  const { id } = req.params;
  Profesional.getEvolucionesByProfesionalId(id)
    .then(results => {
      const evoluciones = results[0];
      return createResponse(res, 200, "Evoluciones encontradas correctamente", "evoluciones", evoluciones);
    })
}

/* POST add profesional /profesionales */
exports.addProfesional = (req, res, next) => {
  const { nombre, apellido, cod_esp } = req.body;
  Profesional.addProfesional(nombre, apellido, cod_esp)
    .then(results => {
      console.log(results[0]);
      if (results[0].affectedRows > 0) {
        return createResponse(
          res,
          200,
          `Profesional agregado correctamente con id: ${results[0].insertId}`,
          "queryResult",
          results[0]
        );
      } else {
        throw new Error("No se pudo agregar el nuevo profesional.");
      }
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};

/* DELETE delete profesional /profesionales/:id */
exports.deleteProfesional = (req, res, next) => {
  const id = parseInt(req.params.id);
  Profesional.deleteProfesional(id)
    .then(results => {
      if (results[0].affectedRows > 0) {
        return createResponse(
          res,
          200,
          `El profesional con id: ${id} fue borrado correctamente`,
          "queryResults",
          results[0]
        );
      } else {
        throw new Error(
          "El profesional con id: " + id + " no se pudo encontrar."
        );
      }
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};

/* PUT update profesional /profesionales/:id */
exports.updateProfesional = (req, res, next) => {
  const { nombre, apellido, cod_esp } = req.body;
  const id = parseInt(req.params.id);
  Profesional.updateProfesional(id, nombre, apellido, cod_esp)
    .then(results => {
      if (results[0].affectedRows > 0) {
        const responseData = {
          queryResults: results[0],
          profesional: {
            id,
            nombre,
            apellido,
            cod_esp
          }
        };
        return createResponse(
          res,
          200,
          `Profesional con id: ${id} actualizado correctamente.`,
          "queryResults",
          responseData
        );
      } else {
        throw new Error("No se pudo actualizar el profesional con id: " + id);
      }
    })
    .catch(err => {
      return createResponse(res, 404, err.message, "error", err);
    });
};
