function createResponse(res, status, message, dataPlaceholder, data) {
  return res.status(status).json({
    status,
    message,
    response: {
      [dataPlaceholder]: data
    }
  })
}

module.exports = createResponse;