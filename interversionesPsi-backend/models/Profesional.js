const c = require("../database/mysql");

class Profesional {
  constructor() {}

  static getProfesionales() {
    return c.execute(
      `SELECT id_prof, nombre_prof, apellido_prof, p.cod_esp, e.nombre_esp FROM profesionales as p 
      INNER JOIN especialidades AS e 
      ON p.cod_esp = e.cod_esp`
    );
  }

  static getProfesionalById(id) {
    return c.execute(
      `SELECT id_prof, nombre_prof, apellido_prof, p.cod_esp, e.nombre_esp 
      FROM profesionales AS p INNER JOIN especialidades as e 
      ON p.cod_esp = e.cod_esp WHERE id_prof = ?`,
      [id]
    );
  }

  static getPacientesByProfesionalId(id) {
    return c.execute(
      `SELECT
        pac.num_hc AS "historia_clinica_paciente", 
        CONCAT(pac.nombre_pac, ' ', pac.apellido_pac) AS 'nombre_paciente',
        pf.id_prof AS "id_profesional", 
        CONCAT(pf.nombre_prof, ' ', pf.apellido_prof) AS 'nombre_profesional'
        FROM pacientes AS pac 
        INNER JOIN hc 
        ON pac.num_hc = hc.num_hc 
        INNER JOIN profesionales AS pf 
        ON pf.id_prof = hc.id_prof   
        WHERE pf.id_prof = ?`,
      [id]
    );
  }

  static getPacientesWithProfesional() {
    return c.execute(
      `SELECT
      pac.num_hc AS "historia_clinica_paciente", 
      CONCAT(pac.nombre_pac, ' ', pac.apellido_pac) AS 'nombre_paciente',
      pf.id_prof AS "id_profesional", 
      CONCAT(pf.nombre_prof, ' ', pf.apellido_prof) AS 'nombre_profesional'  
      FROM pacientes AS pac 
      INNER JOIN hc 
      ON pac.num_hc = hc.num_hc 
      INNER JOIN profesionales AS pf 
      ON pf.id_prof = hc.num_hc`
    );
  }

  static getEvolucionesByProfesionalId(profesional_id) {
    return c.execute(
      `SELECT * FROM profesionales AS pf
    INNER JOIN hc ON pf.id_prof = hc.id_prof
    INNER JOIN pacientes AS pac ON hc.num_hc = pac.num_hc 
    INNER JOIN evoluciones AS e ON e.id_prof = hc.id_prof
    AND e.num_hc = hc.num_hc
    WHERE pf.id_prof = ?
    ORDER BY e.id_evo`,
      [profesional_id]
    );
  }

  static addProfesional(nombre, apellido, cod_esp) {
    return c.execute(
      "INSERT INTO profesionales (nombre_prof, apellido_prof, cod_especialidad) VALUES (?,?,?)",
      [nombre, apellido, cod_esp]
    );
  }

  static deleteProfesional(id) {
    return c.execute("DELETE FROM profesionales WHERE id_prof = ?", [id]);
  }

  static updateProfesional(id, nombre, apellido, cod_esp) {
    return c.execute(
      "UPDATE profesionales set nombre_prof = ?, apellido_prof = ?, cod_especialidad = ? WHERE id_prof = ?",
      [nombre, apellido, cod_esp, id]
    );
  }
}

module.exports = Profesional;
