const c = require("../database/mysql");

class Paciente {

  static getPacientes() {
    return c.execute("SELECT * FROM pacientes");
  }

  static getPacienteById(id) {
    console.log(id);
    console.log(typeof id);
    return c.execute("SELECT * FROM pacientes WHERE num_hc = ?", [id]);
  }

  static addPaciente(nombre, apellido, edad, telefono) {
    return c.execute("INSERT INTO pacientes VALUES (null,?,?,?,?)", [
      nombre,
      apellido,
      edad,
      telefono
    ]);
  }

  static addPacienteToProfesional(id_paciente, id_profesional) {
    return c.execute(`INSERT INTO hc VALUES (?,?)`,[id_paciente,id_profesional]);
  }

  static deletePaciente(id) {
    return c.execute("DELETE FROM pacientes WHERE num_hc = ?", [id]);
  }

  static updatePaciente(id, nombre, apellido, edad, telefono) {
    return c.execute(
      "UPDATE pacientes SET nombre_pac = ?, apellido_pac = ?, edad_pac = ?, telefono_pac = ? WHERE num_hc = ?",
      [nombre, apellido, edad, telefono, id]
    );
  }

  static getEvolucionesByPaciente(id) {
    return c.execute(
      "SELECT * FROM pacientes AS p INNER JOIN hc ON p.num_hc = hc.num_hc INNER JOIN evoluciones AS e ON hc.num_hc = e.num_hc WHERE p.num_hc = ? ORDER BY e.fecha ASC",
      [id]
    );
  }
}

module.exports = Paciente;
