var express = require('express');
// var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

/* importing Routes */
const pacientesRoutes = require('./routes/pacientes');
const profesionalesRoutes = require('./routes/profesionales');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/pacientes', pacientesRoutes);
app.use('/api/profesionales', profesionalesRoutes);

module.exports = app;
