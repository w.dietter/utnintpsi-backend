CREATE DATABASE IF NOT EXISTS consultoriopsi;

USE consultoriopsi;

CREATE TABLE IF NOT EXISTS pacientes
(
	num_hc INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre_pac VARCHAR(255) NOT NULL,
    apellido_pac VARCHAR(255) NOT NULL,
    edad_pac INT NOT NULL,
    telefono_pac VARCHAR(127) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS especialidades
(
	cod_esp INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre_esp VARCHAR(255)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS diagnosticos
(
	cod_diag VARCHAR(255) PRIMARY KEY,
    nombre_diag VARCHAR(255),
	desc_diag LONGTEXT
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS profesionales
(	
	id_prof INT NOT NULL AUTO_INCREMENT,
    nombre_prof VARCHAR(255) NOT NULL,
    apellido_prof VARCHAR(255) NOT NULL,
    cod_esp INT NOT NULL,
    num_matricula INT NOT NULL,
    telefono_prof VARCHAR(127),
    PRIMARY KEY(id_prof),
    FOREIGN KEY(cod_esp) REFERENCES especialidades(cod_esp)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS hc
(	
	num_hc INT NOT NULL,
    id_prof INT NOT NULL,
    PRIMARY KEY(num_hc, id_prof),
    FOREIGN KEY(num_hc) REFERENCES pacientes(num_hc)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
    FOREIGN KEY(id_prof) REFERENCES profesionales(id_prof)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS evoluciones
(
	id_evo INT NOT NULL AUTO_INCREMENT,
    num_hc INT NOT NULL,
    id_prof INT NOT NULL,
    id_diag VARCHAR(255) NOT NULL,
    fecha DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    examen_psi LONGTEXT NOT NULL,
    contenido LONGTEXT NOT NULL,
    PRIMARY KEY(id_evo, num_hc, id_prof),
    FOREIGN KEY(num_hc) REFERENCES hc(num_hc) 
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
    FOREIGN KEY(id_prof) REFERENCES hc(id_prof)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
    FOREIGN KEY(id_diag) REFERENCES diagnosticos(cod_diag)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
) ENGINE=InnoDB;

#dummy data

INSERT INTO diagnosticos
(cod_diag, nombre_diag, desc_diag)
VALUES
("F20", "esquizofrenia", "esquizofrenia"),
("F20.1", "esquizofrenia paranoide", "esquizofrenia paranoide"),
("F66", "trastorno limite de la personalidad", "TLP");

INSERT INTO especialidades
VALUES
(null, "Psicologia"),
(null, "Psiquiatria"),
(null, "Musico terapia"),
(null, "Psicopedagogia");

INSERT INTO profesionales
VALUES
(null, "Diego", "Weinmann", 1, 66123, "1512341234"),
(null, "Juan", "Perez", 2, 154321, "1543214321"),
(null, "Maria", "Garcia", 1, 60987, "1143215678");

INSERT INTO pacientes
VALUES
(null, "Juan", "Juarez", 56, "1109098888"),
(null, "Florencia", "Jutres", 43, "1132122222"),
(null, "Pedro", "Adrefus", 34, "1578674321");

INSERT INTO hc
VALUES
(1, 1),
(2, 1),
(3, 2); 

INSERT INTO evoluciones
VALUES
(null, 1, 1, "F20", default, "Vigil, estable", "paciente tranquilo, estable"),
(null, 2, 1, "F20.1", default,  "Estable, orientado", "paciente asiste a la consulta"),
(null, 1, 1, "F20", default,  "Un poco alterado", "tranquilo");