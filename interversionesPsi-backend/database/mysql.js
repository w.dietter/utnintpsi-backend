const mysql = require('mysql2');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'consultoriopsi',
  password: 'password'
});

module.exports = connection.promise();

