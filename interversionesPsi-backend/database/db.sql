CREATE TABLE pacientes (
  id_pac INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre_pac VARCHAR(255) NOT NULL,
  apellido_pac VARCHAR(255) NOT NULL,
  domicilio_pac VARCHAR(255) NOT NULL
);

CREATE TABLE profesionales(
  id_prof INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre_prof VARCHAR(255) NOT NULL,
  apellido_prof VARCHAR(255) NOT NULL,
  cod_especialidad INT NOT NULL,
  FOREIGN KEY(cod_especialidad)
  REFERENCES especialidades(id_esp)
  ON UPDATE CASCADE
  ON DELETE RESTRICT
);

INSERT INTO profesionales
VALUES
(null, "Diego", "Weinmann", 1),
(null, "Jimena", "Escudero", 1),
(null, "Erika", "Aravena", 1),
(null, "Nicolas", "Marotta", 2),
(null, "Juan Carlos", "Gonzales", 1),
(null, "Nicolas", "Prietto", 2);

CREATE TABLE especialidades(
  id_esp INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre_esp VARCHAR(255)
);

CREATE TABLE pacientesProfesionales(
  id_pac INT NOT NULL,
  id_prof INT NOT NULL,
  primeraConsulta DATE NOT NULL,
  PRIMARY KEY(id_pac, id_prof)
);

QUERYS:

SELECT p.nombre_prof, p.apellido_prof, e.nombre_esp FROM profesionales as p
INNER JOIN especialidades as e
ON p.cod_especialidad = e.id_esp;

/* Listar los pacientes y que profesional los atiende */

SELECT 
CONCAT(pacientes.nombre_pac, " ", pacientes.apellido_pac) AS "nombre paciente",
CONCAT(profesionales.nombre_prof, " ", profesionales.apellido_prof) AS "nombre profesional",
pacientesprofesionales.primeraConsulta AS "Fecha de primera consulta"  
FROM pacientes
INNER JOIN pacientesprofesionales ON pacientes.id_pac = pacientesprofesionales.id_pac
INNER JOIN profesionales ON pacientesprofesionales.id_prof = profesionales.id_prof; 

SELECT 
CONCAT(pacientes.nombre_pac, " ", pacientes.apellido_pac) AS "nombre paciente",
CONCAT(profesionales.nombre_prof, " ", profesionales.apellido_prof) AS "nombre profesional",
pacientesprofesionales.primeraConsulta AS "Fecha de primera consulta"  
FROM pacientes
INNER JOIN pacientesprofesionales ON pacientes.id_pac = pacientesprofesionales.id_pac
INNER JOIN profesionales ON pacientesprofesionales.id_prof = profesionales.id_prof
WHERE profesionales.id_prof = 7; 