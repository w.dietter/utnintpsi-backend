USE intpsi;

-- Generation time: Mon, 13 May 2019 02:14:24 +0000
-- Host: mysql.hostinger.ro
-- DB name: u574849695_25
/*!40030 SET NAMES UTF8 */;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `pacientes`;
CREATE TABLE `pacientes` (
  `id_pac` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_pac` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido_pac` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domicilio_pac` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_pac`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `pacientes` VALUES ('1','Jason','Crist','349 Brown Plain'),
('2','Una','Hansen','10179 Kshlerin Vista'),
('3','Matilda','Prohaska','77253 Gleichner Vista'),
('4','Emory','Watsica','3627 Ricky Parkway'),
('5','Elsie','Jakubowski','6947 Fae River Suite 958'),
('6','Katherine','Schimmel','5646 Irwin Squares'),
('7','Cesar','Lang','39042 Larkin Hill Suite 984'),
('8','Francis','Heaney','3400 Guy Canyon Apt. 222'),
('9','Reilly','Braun','94368 Hane Flats Apt. 469'),
('10','Mervin','Stiedemann','76146 Hipolito Tunnel Suite 280'),
('11','Kareem','Frami','64124 Jazlyn Roads Apt. 174'),
('12','Lelia','Hamill','05348 Antoinette Junction'),
('13','Lavinia','Halvorson','9080 Kerluke Cove'),
('14','Isabel','Russel','968 Kraig Summit Apt. 964'),
('15','Daija','Brakus','207 Keebler Ville Apt. 121'),
('16','Clarabelle','Rogahn','540 Welch Rapids Apt. 457'),
('17','Rosina','Pollich','600 Shanna Cape Suite 104'),
('18','Terry','Gutkowski','986 Hirthe Haven'),
('19','Hank','Hettinger','59149 Sawayn Locks Suite 558'),
('20','Demetris','Lowe','90658 Zula Lodge'); 




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

